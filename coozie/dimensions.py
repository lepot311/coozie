class Dimension(object):
    '''
    A very light wrapper around the pint.util.UnitsContainer class.
    '''

    # pint_dim is an instance of a pint.util.UnitsContainer
    def __init__(self, pint_dim):
        self._pint_dim = pint_dim
        self.units = set()

    def __str__(self):
        return str(self._pint_dim) \
                .replace('[', '')  \
                .replace(']', '')  \
                .replace(' ', '_')

    def __len__(self):
        return len(self._pint_dim)




#for d in find_dimensions(reg, _units):
    #if len(d) == 1:
        #setattr(__module, str(d), d)
        ## set aliases
        #try:
            #for alias in ALIASES[str(d)]:
                #setattr(__module, alias, d)
        #except KeyError:
            #pass























#class Dimension(object):
    #def __init__(self, dimensionality, reg=None):
        #self.dimensionality = dimensionality
        #self.reg = reg

    #def __repr__(self):
        #return repr(self.dimensionality)

    #def __str__(self):
        #return ' / '.join([ meas[0] for meas in self.measurement ])

    #@property
    #def units(self):
        #return self.reg.reg.get_compatible_units()

    #@property
    #def measurement(self):
        #result = []
        #for k, v in sorted(self.dimensionality.items()):
            #sign  = '-' if v < 0 else ''
            #value = k.strip('[]')
            #power = '' if abs(v) == 1 else ' ^ {}'.format(int(abs(v)))

            #key = "{}{}{}".format(
                    #sign,
                    #value,
                    #power,
                #)

            #result.append([
                #key,
                #v,
            #])
        #return result
