from .measurement import Measurement
from .systems import *


class Unit(object):
    def __init__(self, pint_unit, aliases=None):
        self._pint_unit = pint_unit
        self.aliases = aliases or []
        self.dimensions = set()

    def __str__(self):
        return str(self._pint_unit)

    def __mul__(self, value):
        if (isinstance(value, int)
         or isinstance(value, float)):
            return Measurement(unit=self, quantity=value)

    def to(self, target):
        return Measurement(unit=self, quantity=1).to(target)

    @property
    def system(self):
        for system in imperial, metric:
            for dim in system.dimensions:
                if str(self) in system._dimensions[dim]:
                    return system

    @property
    def sys(self):
        return self.system
