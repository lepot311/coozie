from .systems import MeasurementSystem


class Measurement:
    def __init__(self, unit=None, quantity=None):
        self.unit     = unit
        self.quantity = float(quantity)

        self._pint_quantity = self.unit._pint_unit * self.quantity

    def __repr__(self):
        return f"Measurement(unit={self.unit}, quantity={self.quantity})"

    def __mul__(self, other):
        if isinstance(other, Measurement):
            if other.unit == self.unit:
                return Measurement(
                    unit=self.unit,
                    quantity=self.quantity * other.quantity,
                )

    def __add__(self, other):
        if isinstance(other, Measurement):
            if other.unit == self.unit:
                return Measurement(
                    unit=self.unit,
                    quantity=self.quantity + other.quantity,
                )
            else:
                # TODO check for same dimension
                return CompoundMeasurement(
                    measurements=[self, other]
                )


    def to(self, target):
        if isinstance(target, MeasurementSystem):
            return 'TODO'
        #elif isinstance(target, Unit):  # circular import
        else:
            result = self._pint_quantity.to(target._pint_unit)
            return Measurement(
                    unit=target,
                    quantity=result.magnitude,
                )


class CompoundMeasurement(Measurement):
    def __init__(self, measurements=None):
        # TODO these should be ordered by unit magnitude
        self.measurements = measurements

    def __repr__(self):
        return f"CompoundMeasurement(measurements={self.measurements})"

    def __iter__(self):
        return ( m for m in self.measurements )
