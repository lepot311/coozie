from collections import defaultdict

from pint import UnitRegistry
from pint.unit import _Unit
from pint.util import UnitsContainer


from .dimensions import Dimension
from .units import Unit

from .systems import *


class CoozieContainer(dict):
    def __setattr__(self, name, value):
        self[name] = value

    def __getattr__(self, name):
        return self[name]

    def __iter__(self):
        return iter(self.values())


class CoozieRegistry(object):
    ALIASES = {
        'dimensions': {
            'length': ('distance',),
        },
        'units': {
            'mile': ('mi',),
            'meter': ('m',),
        },
    }

    def __init__(self):
        self._registry = UnitRegistry()

        # find unit and dimension defs in Pint registry
        self._unit_defs, self._dim_defs = self._parse_pint()

        # find available dimensions
        self.dimensions = self.dim = CoozieContainer()
        for d in self._dim_defs:
            dim = Dimension(d)
            setattr(self.dimensions, str(dim), dim)

            # set aliases
            try:
                aliases = __class__.ALIASES['dimensions'][str(dim)]
            except KeyError:
                pass
            else:
                for name in aliases:
                    setattr(self.dimensions, name, dim)



        # initialize units with dimensions
        self.units = self.u = CoozieContainer()
        for k, v in self._unit_defs.items():
            unit = Unit(k, aliases=v)
            setattr(self.units, str(unit), unit)

            # set aliases
            try:
                aliases = __class__.ALIASES['units'][str(unit)]
            except KeyError:
                pass
            else:
                for name in aliases:
                    setattr(self.units, name, unit)


        # link units to dimensions and vise versa
        self._link_units_dimensions()

        # systems have units
        self.systems = self.sys = CoozieContainer()
        self.systems.imperial = imperial
        self.systems.metric   = metric

        self._populate_units_namespace(self.units)


    def _populate_units_namespace(self, units):
        '''
        Take a list of initialized Units and smash them into the coozie.units
        module namespace. This allows us to reference a unit instance like:
            a_foot = coozie.units.foot
            4_feet = coozie.units.foot * 4
        '''
        # fun with namespaces!
        import sys
        __module = sys.modules['coozie.units']

        for unit in units:
            for alias in unit.aliases:
                setattr(__module, alias, unit)


    def _link_units_dimensions(self):
        for unit in self.units:
            # find the unit's Coozie Dimension
            for dim in self.dimensions:
                if unit._pint_unit.dimensionality == dim._pint_dim:
                    # bidirectional link
                    unit.dimensions.add(dim)
                    dim.units.add(unit)

    def _parse_pint(self):
        '''
        Inspect the members of the Pint registry and return a dictionary of
            {
                _Unit(): [aliases,..],
            }
        for each one if the member is both a Pint _Unit and has a dimensionality
        '''
        aliases    = defaultdict(set)
        dimensions = defaultdict(set)

        for name in dir(self._registry):
            instance = getattr(self._registry, name)

            if isinstance(instance, _Unit):
                aliases[instance].add(name)
                try:
                    dim = instance.dimensionality
                    # only include non-empty `UnitsContainer`s
                    if isinstance(dim, UnitsContainer) and len(dim) != 0:
                        dimensions[dim].add(name)
                except AttributeError:
                    continue

        return aliases, dimensions
