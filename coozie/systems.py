from collections import defaultdict


class MeasurementSystem(object):
    def __init__(self, **kwargs):
        self._dimensions = kwargs

    @property
    def dimensions(self):
        return list(self._dimensions.keys())



imperial = MeasurementSystem(
    length=[
        'mile',
        'yard',
        'foot',
        'inch',
    ],
    mass=[
        'ton',
        'pound',
        'ounce',
    ],
)

metric = MeasurementSystem(
    length=[
        'kilometer',
        'meter',
        'centimeter',
        'millimeter',
    ],
    mass=[
        'kilogram',
        'gram',
    ],
)
