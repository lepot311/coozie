Coozie
===============================

version number: 0.0.1
author: Erik Potter

## Purpose
Converting between units of measurement in Python should be easy if not enjoyable. Coozie aims to find a place in your list of "must-have" Python packages.

## What it is
Coozie is a package that wraps the Pint unit conversion package. It aims to provide an easy to use API around Pint.

## How to use it

### Measuring things

#### Simple measurements
```
a_foot = coozie.units.foot

print(a_foot.unit)
> coozie.units.foot

print(a_foot.quantity)
> 1.0
```

#### Mathematical operations on simple measurements
```
four_feet = coozie.units.foot * 4
eight_feet = four_feet + four_feet

print(eight_feet)
> coozie.Measurement(unit=foot, quantity=8.0)

sixteen_feet = four_feet * four_feet

print(sixteen_feet)
> coozie.Measurement(unit=foot, quantity=16.0)
```


#### Converting between units
```
one_foot = coozie.units.foot

print(one_foot.to(coozie.units.inch))
> coozie.Measurement(unit=inch, quantity=12.0)
```


#### Combining disparate units
```
height = (coozie.units.foot * 5) + (coozie.units.inch * 8)
feet, inches = height

print(feet)
> coozie.Measurement(unit=foot, quantity=5.0)

print(inches)
> coozie.Measurement(unit=ince, quantity=8.0)
```
