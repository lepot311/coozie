from unittest import TestCase

from nose.plugins.skip import SkipTest

import coozie
from coozie.measurement import Measurement, CompoundMeasurement
from coozie.systems import MeasurementSystem
from coozie.units import Unit


class CooziePintTest(TestCase):
    def test_dimension_count(self):
        self.assertEqual(
            len(coozie.reg.dimensions),
            47,
        )

    def test_unit_count(self):
        self.assertEqual(
            len(coozie.reg.units),
            270,
        )


class CoozieModuleImportTest(TestCase):
    def test_import_dimension(self):
        raise SkipTest()
        from coozie.reg.dimensions import distance
        assert distance

    def test_import_unit(self):
        '''
        You should be able to get a Unit instance from the coozie.units module.
        '''
        assert coozie.units.mi
        assert isinstance(coozie.units.mi, Unit)
        assert coozie.units.mile
        assert isinstance(coozie.units.mile, Unit)


class CoozieAliasTest(TestCase):
    def test_dimension_aliases(self):
        self.assertEqual(
            coozie.reg.dim.length,
            coozie.reg.dim.distance,
        )

    def test_unit_aliases(self):
        self.assertEqual(
            coozie.reg.u.mile,
            coozie.reg.u.mi,
        )
        self.assertEqual(
            coozie.reg.u.meter,
            coozie.reg.u.m,
        )


class CoozieSystemMembershipTest(TestCase):
    def test_unit_system_type(self):
        self.assertTrue(
            isinstance(coozie.reg.u.mile.system,  MeasurementSystem)
        )
        self.assertTrue(
            isinstance(coozie.reg.u.meter.system,  MeasurementSystem)
        )

    def test_unit_system_membership(self):
        self.assertEqual(
            coozie.reg.u.mile.system,
            coozie.reg.sys.imperial,
        )
        self.assertEqual(
            coozie.reg.u.meter.system,
            coozie.reg.sys.metric,
        )


class CoozieMeasurementTest(TestCase):
    def test_measurement_defaults_to_magnitude_1(self):
        '''
        If a unit is specified in a mathmatical operation without a quantity,
        we should assume the unit has a quantity of 1.0
        '''
        one_foot = coozie.units.foot
        four_feet = one_foot * 4
        assert isinstance(four_feet, Measurement)
        assert four_feet.quantity == 4

    def test_add_like_measurements(self):
        '''
        Adding two measurements of the same unit should result in a new
        measurement of that unit whose quantity is the sum of both
        measurements.
        '''
        a = coozie.units.foot * 2
        b = coozie.units.foot * 4
        total = a + b
        assert isinstance(total, Measurement)
        assert total.unit == coozie.units.foot
        assert total.quantity == 6

    def test_mul_like_measurements(self):
        '''
        Multiplying two measurements of the same unit should result in a new
        measurement of that unit whose quantity is the product of both
        measurements.
        '''
        a = coozie.units.foot * 2
        b = coozie.units.foot * 4
        total = a * b
        assert isinstance(total, Measurement)
        assert total.unit == coozie.units.foot
        assert total.quantity == 8

    def test_compound_measurements(self):
        '''
        Adding two measurements of different types should result in a compound
        measurement that can be unpacked in order.
        '''
        feet = coozie.units.foot * 5
        inches = coozie.units.inch * 8

        height = feet + inches
        assert isinstance(height, CompoundMeasurement)

        assert feet   in height.measurements
        assert inches in height.measurements

        feet, inches = height
        assert feet.quantity   == 5
        assert inches.quantity == 8


class CoozieConversionTest(TestCase):
    def test_dimension_convert_unit(self):
        a_foot = coozie.units.foot
        foot_inches = a_foot.to(coozie.units.inch)
        assert foot_inches.quantity == 12.0

    def test_dimension_convert_system(self):
        raise SkipTest()
        distance = coozie.u.mile * 1.0
        metric_distance = distance.to(coozie.sys.metric)

        assert metric_distance.u   == coozie.u.meter
        assert metric_distance.sys == coozie.sys.metric
        assert metric_distance.mag == 1609.34
